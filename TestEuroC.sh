
# delay between launching various modules
module_delay=1.5

screen -d -m -S roscore bash -c "
	source devel/setup.bash; 
	roscore; 
	exec bash -i"

sleep ${module_delay}
sleep ${module_delay}

screen -d -m -S msckf_vio bash -c " 
	source devel/setup.bash; 
	roslaunch msckf_vio msckf_vio_euroc.launch; 
	exec bash -i"

sleep ${module_delay}
sleep ${module_delay}
sleep ${module_delay}

screen -d -m -S rviz bash -c " 
	source devel/setup.bash; 
	rviz; 
	exec bash -i"

cd .. && cd ORB_SLAM2

rosbag play --pause V1_01_easy.bag

