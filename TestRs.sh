# Realsense Launcher with 
# - Roscore
# - Victor's MSCKF_VIO with loop closure 
# - RVIZ visualization
# - Rosbag play

# delay between launching various modules
module_delay=1.5

screen -d -m -S roscore bash -c "
	source devel/setup.bash; 
	roscore; 
	exec bash -i"

sleep ${module_delay}
sleep ${module_delay}
sleep ${module_delay}
sleep ${module_delay}

screen -d -m -S msckf_vio bash -c " 
	source devel/setup.bash; 
	roslaunch msckf_vio msckf_vio_realsense.launch; 
	exec bash -i"

sleep ${module_delay}
sleep ${module_delay}
sleep ${module_delay}


screen -d -m -S rviz bash -c " 
	source devel/setup.bash; 
	rosrun rviz rviz -d ~/msckf_ws/src/msckf_vio/rviz/rviz_realsense_config.rviz; 
	exec bash -i"

rosbag play --pause bag/D435i/med2_D435i.bag -r 0.3

