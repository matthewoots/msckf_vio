#ifndef GPSUTILS_H
#define GPSUTILS_H

#include <math.h>

/**
 * @brief Some helpers for converting GPS readings from the WGS84 geodetic system to a
*local North-East-Up cartesian axis.

*The implementation here is according to the paper:
*"Conversion of Geodetic coordinates to the Local Tangent Plane" Version 2.01.
*"The basic reference for this paper is J.Farrell & M.Barth 'The Global
*Positioning System & Inertial Navigation'" Also helpful is Wikipedia:
*http://en.wikipedia.org/wiki/Geodetic_datum
*/
class GpsUtils
{
    // WGS-84 geodetic constants
public:
    GpsUtils()
    {
        a = 6378137;        // WGS-84 Earth semimajor axis (m)
        b = 6356752.3142;   //WGS-84 Earth semiminor axis (m)
        f = (a - b) / a;    // Ellipsoid Flatness
        e_sq = f * (2 - f); //Square of Eccentricity
    }
    ~GpsUtils() {}

    /**
     * @brief Converts WGS-84 Geodetic point (lat, lon, h) to the
     * Earth-Centered Earth-Fixed (ECEF) coordinates (x, y, z).
     */
    void GeodeticToEcef(double lat, double lon, double h)
    {
        // Convert to radians in notation consistent with the paper:
        double lambda = DegreesToRadians(lat);
        double phi = DegreesToRadians(lon);
        double s = sin(lambda);
        double N = a / sqrt(1 - e_sq * s * s);

        double sin_lambda = sin(lambda);
        double cos_lambda = cos(lambda);
        double cos_phi = cos(phi);
        double sin_phi = sin(phi);

        xEcef = (h + N) * cos_lambda * cos_phi;
        yEcef = (h + N) * cos_lambda * sin_phi;
        zEcef = (h + (1 - e_sq) * N) * sin_lambda;
    }

    /** 
     * @brief Converts the Earth-Centered Earth-Fixed (ECEF) coordinates (x, y, z) to
    * East-North-Up coordinates in a Local Tangent Plane that is centered at
    * the (WGS-84) Geodetic point (lat0, lon0, h0).
    */
    void EcefToEnu(double lat0, double lon0, double h0)
    {
        // Convert to radians in notation consistent with the paper:
        double lambda = DegreesToRadians(lat0);
        double phi = DegreesToRadians(lon0);
        double s = sin(lambda);
        double N = a / sqrt(1 - e_sq * s * s);

        double sin_lambda = sin(lambda);
        double cos_lambda = cos(lambda);
        double cos_phi = cos(phi);
        double sin_phi = sin(phi);

        double x0 = (h0 + N) * cos_lambda * cos_phi;
        double y0 = (h0 + N) * cos_lambda * sin_phi;
        double z0 = (h0 + (1 - e_sq) * N) * sin_lambda;

        double xd, yd, zd;
        xd = xEcef - x0;
        yd = yEcef - y0;
        zd = zEcef - z0;

        // This is the matrix multiplication
        xEast = -sin_phi * xd + cos_phi * yd;
        yNorth = -cos_phi * sin_lambda * xd - sin_lambda * sin_phi * yd +
                 cos_lambda * zd;
        zUp = cos_lambda * cos_phi * xd + cos_lambda * sin_phi * yd +
              sin_lambda * zd;
    }

    /**@brief Converts the geodetic WGS-84 coordinated (lat, lon, h) to
    * East-North-Up coordinates in a Local Tangent Plane that is centered at
    * the (WGS-84) Geodetic point (lat0, lon0, h0).
    */
    void GeodeticToEnu(double lat, double lon, double h, double lat0,
                       double lon0, double h0)
    {
        GeodeticToEcef(lat, lon, h);
        EcefToEnu(lat0, lon0, h0);
    }
/**
 * @brief check whether two values are too close (less than 1e-2)
 */
    bool AreClose(double x0, double x1)
    {
        double d = x1 - x0;
        return (d * d) < 1e-2;
    }
/**
 * @brief convert degrees to radians
 */
    double DegreesToRadians(double degrees) { return M_PI / 180.0 * degrees; }
/**
 * @brief convert radians to degrees
 */
    double RadiansToDegrees(double radians) { return 180.0 / M_PI * radians; }
    double xEast; ///< East-North-Up coordinates x in a Local Tangent Plane
    double yNorth;///< East-North-Up coordinates y in a Local Tangent Plane
    double zUp;///< East-North-Up coordinates z in a Local Tangent Plane

private:
    double a;    ///< WGS-84 Earth semimajor axis (m)
    double b;    ///< WGS-84 Earth semiminor axis (m)
    double f;    ///< Ellipsoid Flatness
    double e_sq; ///< Square of Eccentricity
    double xEcef; ///<Earth-Centered Earth-Fixed (ECEF) coordinates x.
    double yEcef; ///<Earth-Centered Earth-Fixed (ECEF) coordinates y.
    double zEcef; ///<Earth-Centered Earth-Fixed (ECEF) coordinates z.
};

#endif